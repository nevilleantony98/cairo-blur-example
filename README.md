# CairoBlur

A sample blur app reusing gtk's cairo blur used for shadows.

## Usage:
```sh
blur [blur-radius] [image-path]
```

*blur-radius: integer (default = 20px)*
*image-path: path to image*