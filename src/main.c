#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdint.h>
#include "cairoblur.h"

typedef struct {
  GdkPixbuf *pix;
  int radius;
} CairoBlur;

static gboolean
draw_cb (GtkWidget *widget, cairo_t *cr, CairoBlur *cairoblur)
{
  cairo_surface_t *surface = gdk_cairo_surface_create_from_pixbuf (cairoblur->pix, 1, NULL);

  _gtk_cairo_blur_surface (surface, cairoblur->radius);
  cairo_scale (cr, 0.5, 0.5);
  cairo_set_source_surface (cr, surface, 0, 0);
  cairo_paint (cr);

  return FALSE;
}

static void
activate (GtkApplication *app,
          char          **argv)
{
  GtkWidget *window;
  GtkWidget *canvas;
  CairoBlur *cairoblur;
  int width, height;
  char *sample_cover = "/org/gnome/gitlab/nevilleantony98/CairoBlur/cover.png";
  GError *err = NULL;

  cairoblur = g_malloc (sizeof (CairoBlur));
  cairoblur->radius = 20;

  cairoblur->pix = gdk_pixbuf_new_from_resource (sample_cover, &err);

  if (argv[1] != NULL) {
    cairoblur->radius = atoi (argv[1]);

    if (argv[2] != NULL)
      cairoblur->pix = gdk_pixbuf_new_from_file (argv[2], &err);
  }

  if (err) {
      printf("Error : %s\n", err->message);
      g_error_free (err);
      return;
  }

  width = gdk_pixbuf_get_width (cairoblur->pix);
  height = gdk_pixbuf_get_height (cairoblur->pix);

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Cairo Blur");
  gtk_window_set_default_size (GTK_WINDOW (window), width/2, height/2);

  canvas = gtk_drawing_area_new ();
  gtk_container_add (GTK_CONTAINER (window), canvas);
  g_signal_connect (canvas, "draw", G_CALLBACK (draw_cb), cairoblur);

  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gnome.gitlab.nevilleantony98.CairoBlur", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), argv);
  status = g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref (app);

  return status;
}